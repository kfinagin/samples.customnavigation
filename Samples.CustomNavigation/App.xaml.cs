﻿namespace Samples.CustomNavigation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using Windows.ApplicationModel.Activation;
    using Caliburn.Micro;
    using Microsoft.Practices.Unity;
    using Samples.CustomNavigation.Helpers;
    using Samples.CustomNavigation.UILogic.ViewModels;
    using Samples.CustomNavigation.UILogic.ViewModels.Displayed;

    sealed partial class App : CaliburnApplication
    {
        private UnityContainer container;

        public App()
        {
            InitializeComponent();
        }

        protected override IEnumerable<Assembly> SelectAssemblies()
        {
            var assemblies = base.SelectAssemblies().ToList();
            assemblies.Add(typeof(ShellViewModel).GetTypeInfo().Assembly);

            return assemblies;
        }

        protected override void Configure()
        {
            ConfigureTypeMappings();

            this.container = new UnityContainer();
            this.container.RegisterContainerControlled<IEventAggregator, EventAggregator>();
            this.container.RegisterContainerControlled<ViewModelFactory>();
            this.container.RegisterPerResolve<ShellViewModel>();
        }

        private void ConfigureTypeMappings()
        {
            var config = new TypeMappingConfiguration
            {
                DefaultSubNamespaceForViews = "Samples.CustomNavigation.Views",
                DefaultSubNamespaceForViewModels = "Samples.CustomNavigation.UILogic.ViewModels"
            };

            ViewLocator.ConfigureTypeMappings(config);
            ViewModelLocator.ConfigureTypeMappings(config);
        }

        protected override object GetInstance(Type service, string key)
        {
            if (service != null)
            {
                return this.container.Resolve(service);
            }

            if (!string.IsNullOrWhiteSpace(key))
            {
                return this.container.Resolve(Type.GetType(key));
            }

            return null;
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return this.container.ResolveAll(service);
        }

        protected override void BuildUp(object instance)
        {
            this.container.BuildUp(instance);
        }

        protected override void OnLaunched(LaunchActivatedEventArgs args)
        {
            DisplayRootViewFor<ShellViewModel>();
        }
    }
}
