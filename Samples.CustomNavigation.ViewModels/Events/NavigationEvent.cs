﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Samples.CustomNavigation.UILogic.Events
{
    public class NavigationEvent
    {
        public string Parameter { get; set; }
    }
}
