﻿namespace Samples.CustomNavigation.UILogic.ViewModels.Displayed
{
    using System;
    using Caliburn.Micro;

    public class ViewModelFactory
    {
        public IScreen GetViewModel(string id)
        {
            switch (id)
            {
                case "Active1":
                    return new Active1ViewModel();
                case "Active2":
                    return new Active2ViewModel();
                case "Home":
                    return new HomeViewModel();
                case "Overlay":
                    return new OverlayViewModel();
                default:
                    throw new ArgumentException("Invalid parameter");
            }
        }

    }
}
