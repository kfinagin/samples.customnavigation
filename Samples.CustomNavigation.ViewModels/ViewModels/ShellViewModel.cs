﻿namespace Samples.CustomNavigation.UILogic.ViewModels
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using Caliburn.Micro;
    using Samples.CustomNavigation.UILogic.Classes;
    using Samples.CustomNavigation.UILogic.Events;
    using Samples.CustomNavigation.UILogic.ViewModels.Displayed;

    public class ShellViewModel : 
        Conductor<IScreen>.Collection.AllActive,
        IHandle<NavigationEvent>
    {
        private readonly IEventAggregator eventAggregator;
        private readonly ViewModelFactory viewModelFactory;

        private readonly Stack<IScreen> navigationBackStack = new Stack<IScreen>();

        public ShellViewModel(IEventAggregator eventAggregator, ViewModelFactory viewModelFactory)
        {
            this.eventAggregator = eventAggregator;
            this.viewModelFactory = viewModelFactory;

            NavigationMenuItems = new ObservableCollection<NavigationMenuItem>();
            this.eventAggregator.Subscribe(this);
        }

        protected override void OnActivate()
        {
            CreateNavigationMenu();
        }

        private void CreateNavigationMenu()
        {
            NavigationMenuItems.Add(new NavigationMenuItem()
            {
                Name = "Home Page",
                NavigationEvent = new NavigationEvent() { Parameter = "Home" }
            }); 
            
            NavigationMenuItems.Add(new NavigationMenuItem()
            {
                Name = "Content Page 1",
                NavigationEvent = new NavigationEvent() {Parameter = "Active1"}
            });
            
            NavigationMenuItems.Add(new NavigationMenuItem()
            {
                Name = "Content Page 2",
                NavigationEvent = new NavigationEvent() { Parameter = "Active2" }
            }); 
        }

        public ObservableCollection<NavigationMenuItem> NavigationMenuItems { get; set; } 

        public IScreen ActiveItemViewModel { get; set; }
        public IScreen OverlayViewModel { get; set; }

        public bool IsBackButtonVisible { get; set; }

        public void MenuItemClicked(NavigationMenuItem item)
        {
            NavigateTo(item.NavigationEvent);
        }

        public void Handle(NavigationEvent navEvent)
        {
            NavigateTo(navEvent);
        }

        public void NavigateBack()
        {
            if (this.navigationBackStack.Count > 0 && this.navigationBackStack.Peek() != ActiveItemViewModel)
            {
                var item = this.navigationBackStack.Pop();
                ActiveItemViewModel = item;
            }

            if (this.navigationBackStack.Count == 0)
            {
                IsBackButtonVisible = false;
            }
        }

        private void NavigateTo(NavigationEvent navEvent)
        {
            //pushing current view model to navigation stack
            this.navigationBackStack.Push(ActiveItemViewModel);

            var viewModel = this.viewModelFactory.GetViewModel(navEvent.Parameter);

            //clearing stack if we go to the home page
            if (viewModel is HomeViewModel)
            {
                this.navigationBackStack.Clear();
                IsBackButtonVisible = false;
            }
            else
            {
                IsBackButtonVisible = true;
            }

            ActiveItemViewModel = viewModel;
        }
    }
}
