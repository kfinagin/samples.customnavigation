﻿namespace Samples.CustomNavigation.UILogic.Classes
{
    using Samples.CustomNavigation.UILogic.Events;

    public class NavigationMenuItem
    {
        public string Name { get; set; }
        public NavigationEvent NavigationEvent { get; set; }
    }
}
